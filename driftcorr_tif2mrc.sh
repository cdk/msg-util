#! /bin/bash
#this script converts and drift corrects tif files 
#boiler plate of function defs, var declarations, and argument processing until line80 

#provide clip and dosefgpu_driftcorr  by setting required PATH etc
source /software/config/imod/IMOD-linux.sh
source /software/config/dosefgpu_driftcorr/dosefgpu_driftcorr.sh

#string to absolute filepath
get_abs_filename() {
  # $1 : relative filename
  #USAGE:
  #absoluteName=$(get_abs_filename filepath)
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

#filename without path, with or without extension
get_plain_name () {
   #$1 : filename
   #$2 : tif extension we don't need, including the dot 
   #USAGE: 
   #plainName=$(get_plain_name filepath fileextension) 
   base=$(basename "$1" "$2")
   echo "$base"
}

#these are variables are for capturing argument values, sesible defaults
tifdirectory='.'         # -t the directory where the tif files are stored
mrcdirectory='.'         # -o the directory to output the corrected mrc files
clip_n=1               # -n the n argument for clip, presumably iterations.
clip_m=2               # -m the output mode for clip
drift_index_start=0 	# -i the starting index option for clip
drift_index_end=0       # -e the endig index option for clip
tif_ext='.tif'              # -x the tif file extension, .tif
tif_pat=''              # -p a filename pattern

#proceses command line arguments
#-h option that prints the expected usage patterns
while getopts ht:o:c:n:m:i:e:g:x:p:  opt; do
  case $opt in
  t)
      tifdirectory=$OPTARG
      ;;
  o)
      mrcdirectory=$OPTARG
      ;;
  n)
      clip_n=$OPTARG
      ;;
  m)
      clip_m=$OPTARG
      ;;
  i)
      drift_index_start=$OPTARG
      ;;
  e)
      drift_index_end=$OPTARG
      ;;
  g)
      clip_gainref=$OPTARG
      ;;
  x)
      tif_ext=$OPTARG
      ;;
  p)
      tif_pat=$OPTARG
      ;;
  h)
      echo "tif2mrc -t TifDirectory -o mrcDirectory -n clipN -m clipM -i driftIndexStart -e driftIndexStop -g clipGainReference -x tifFileExtension -p Tif_Pat_Tern "
	  echo "example:"
	  echo "tif2mrc.sh -t /home/ckennedy/testing_tif2mrc/ -o /home/ckennedy/testing/ -n 1 -m 2 -g /home/ckennedy/testing_tif2mrc/gain.tif  -x .tif -p May03_18.21.27  -i 1 -e 2346"
      exit 0 
      ;; 
  esac
done
shift $((OPTIND-1))
#end of argument processing,scripting starts below this line

#need absolute tif and mrc directory, cuz dosefgpu lacks output options 
tifD=$(get_abs_filename "$tifdirectory")
mrcD=$(get_abs_filename "$mrcdirectory")
#AFAIK, dosefgpu_driftcorr has to output where it runs, so 
#change directory to where we want the output to go
cd "$mrcD"
#Processing of tif files starts here 
let counter=1
for tif in $tifD/*${tif_pat}*${tif_ext}; do
	#batch support is to skip converting until the counter is greater than or equal to drift_index_start
	if [ "$counter" -lt "$drift_index_start" ];then
		echo "skipping $tif,counter=$counter,startingAt $drift_index_start"
        let counter++
		continue
	fi
	echo "processing $tif"
	absoluteTifPath=$(get_abs_filename "$tif" )
	plainName=$(get_plain_name "$tif" "$tif_ext")
	#clip multiply -n "$clip_n" -m "$clip_m" "$absolutePath" "$tifD"/"$clip_gainref" "$mrcdirectory"/"$plainName".mrc
	#use RAMFS for intermediate mrc file, then driftcorrect out of RAM
	clip multiply -n "$clip_n" -m "$clip_m" "$absoluteTifPath" "$tifD"/"$clip_gainref" /dev/shm/"$plainName".mrc
	echo "dosefgpu_driftcorr /dev/shm/${plainName}.mrc -bin 2 -bft 400 -fod 10 -nps 1 -srs 0 -ssc 0 -scc 1 -pbx 128 -gpu 0 -ssr 0"
	dosefgpu_driftcorr "/dev/shm/${plainName}.mrc" -bin 2 -bft 400 -fod 10 -nps 1 -srs 0 -ssc 0 -scc 1 -pbx 128 -gpu 0 -ssr 0 
	echo "rm /dev/shm/${plainName}.mrc"
	rm  "/dev/shm/${plainName}"*
	let counter++
	if [ "$counter" -gt "$drift_index_end" ];then
		echo "reached last index, exiting"
		exit 0
	fi
done
